package slackservice

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/slack-service/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.slack-service", loggo.INFO)
}
