package main

import "gitlab.com/mvenezia/slack-service/cmd/slack-service/cmd"

func main() {
	cmd.Execute()
}
